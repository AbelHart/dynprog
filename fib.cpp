#include <iostream>
#include <vector>

using namespace std;

unsigned long fibR(unsigned n)//recursive fibonacci implementation
{
  
  //... 
  
}



unsigned long fibDP(unsigned n)//dynamic programming fibonacci implementation
{
  
  //... 
  
}


int main()
{

  unsigned input;

  
   cerr<<"Welcome to \"Fibonacci Comparison Program\". We first need some input :  ";

    cin>>input;

    cerr<<endl<<"fibDP("<<input<<") =  "<<fibDP(input)<<endl;

    cerr<<endl<<"fibR("<<input<<") =  "<<fibR(input)<<endl;

    return 0;
}
